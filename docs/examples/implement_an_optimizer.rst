..
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

Implement an Optimizer
======================

.. literalinclude:: /../examples/implement_an_optimizer.py
   :linenos:
   :lines: 6-
