..
    SPDX-FileCopyrightText: 2023-2024 GSI Helmholtzzentrum für Schwerionenforschung
    SPDX-FileNotice: All rights not expressly granted are reserved.

    SPDX-License-Identifier: GPL-3.0-or-later OR EUPL-1.2+

API Reference
=============

.. automodule:: cernml.optimizers

.. toctree::
   :maxdepth: 2

   interface
   coi_integration
   registration
   third_party_wrappers
